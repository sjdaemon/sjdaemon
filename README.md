# README #

This is test multi-threaded web server

### What is this repository for? ###

* Test Project
* 0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
  You will need to install Apache Maven and Java 8.
  To build execute "mvn clean install"
  To run execute com.rgendler.server.ServerRunner from your development environment

* Configuration
  Default "config.properties" included in src/main/resources
  You can either modify server properties in src/main/resources or include
* Dependencies
  Must be able to access public Maven repositories to satisfy dependencies in pom.xml

* Use of third-party code
  No third party code is copied into this project.
  Some ideas used from the following development blog:
  http://tutorials.jenkov.com/java-multithreaded-servers/thread-pooled-server.html

* How to run tests

  Standard Maven command "mvn test"

* Deployment instructions

  To start the server:  java -jar ./target/basic-http-server-1.0-jar-with-dependencies.jar  <config.properties>

*  Limitations

   Only HTTP GET operation is supported
   Only static content is supported - no URL parameters are parsed
   Only text and html files are supported
   To access file  http://localhost:<port>/some/path/to/resource.html
   You must have a file present in <server_root>/some/path/to/resource.html
   HTTP codes supported 200, 403, 404, 500

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Rosti Gendler rosti.gendler@gmail.com