package com.rgendler.http.request;

import com.rgendler.http.request.HttpGetProcessorImpl;
import com.rgendler.http.request.RequestProcessorFactory;
import com.rgendler.server.util.HttpMethod;
import org.junit.Assert;
import org.junit.Test;

/**
 * Test class for {@link RequestProcessorFactory}
 *
 * Created by rgendler on 3/19/2017.
 */
public class RequestProcessorFactoryTest {

    /**
     * Test supported response processor
     */
    @Test
    public void testGetResponseProcessor() {
        Assert.assertNotNull(RequestProcessorFactory.getInstance());
        Assert.assertTrue(RequestProcessorFactory.getInstance()
                .getResponseProcessor(HttpMethod.GET) instanceof HttpGetProcessorImpl);
    }

    /**
     * Test unsupported response processor
     */
    @Test(expected = UnsupportedOperationException.class)
    public void testGetResponseProcessorUnsupported() {
        Assert.assertNotNull(RequestProcessorFactory.getInstance());
        Assert.assertTrue(RequestProcessorFactory.getInstance()
                .getResponseProcessor(HttpMethod.POST) instanceof HttpGetProcessorImpl);
    }
}
