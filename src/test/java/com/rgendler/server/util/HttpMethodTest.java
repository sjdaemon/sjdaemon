package com.rgendler.server.util;

import org.junit.Assert;
import org.junit.Test;

/**
 * Test class for {@link com.rgendler.server.util.HttpMethod}
 *
 * Created by rgendler on 3/17/2017.
 */
public class HttpMethodTest {

    /**
     * Test HttpMethod.getByName() method
     */
    @Test
    public void testGetByName() {
        Assert.assertEquals(HttpMethod.GET, HttpMethod.getByName("GET"));
        Assert.assertEquals(HttpMethod.POST, HttpMethod.getByName("POST"));
        Assert.assertEquals(HttpMethod.DELETE, HttpMethod.getByName("DELETE"));
        Assert.assertEquals(HttpMethod.HEAD, HttpMethod.getByName("HEAD"));
        Assert.assertEquals(HttpMethod.PATCH, HttpMethod.getByName("PATCH"));
        Assert.assertEquals(HttpMethod.PUT, HttpMethod.getByName("PUT"));
    }
}
