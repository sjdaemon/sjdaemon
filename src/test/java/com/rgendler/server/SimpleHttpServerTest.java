package com.rgendler.server;

import org.junit.Test;

import java.util.Properties;

/**
 * Test class for {@link BasicHttpServer}
 * Created by rgendler on 3/11/17.
 */
public class SimpleHttpServerTest {

    @Test
    public void testSimpleHttpServer() throws Exception {
        Properties properties = new Properties();
        properties.setProperty("server.port", "8080");
        properties.setProperty("max.request.count", "10");
        BasicHttpServer server = new BasicHttpServer(properties);
        new Thread(server).start();
        Thread.sleep(1000);
        server.shutdown();
    }

}
