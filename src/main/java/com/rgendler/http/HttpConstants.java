package com.rgendler.http;

/**
 * HTTP Constants
 *
 * Created by rgendler on 3/17/2017.
 */
public class HttpConstants {

    public static final String NOT_FOUND_HTML=
            "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n"+
            "<head>\n"+
            "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\"/>\n"+
            "<title>404 - File or directory not found.</title>\n" +
            "</head>\n" +
            "<body>\n" +
            "<div id=\"header\"><h1>Server Error</h1></div>\n" +
            "<div id=\"content\">\n" +
            "<div class=\"content-container\"><fieldset>\n" +
            "<h2>404 - File or directory not found.</h2>\n" +
            "<h3>The resource you are looking for might have been removed, had its name changed, or is temporarily unavailable.</h3>\n" +
            "</fieldset></div>\n" +
            "</div>\n" +
            "</body>\n" +
            "</html>\n";

    public static final String FORBIDDEN_HTML=
            "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n"+
                    "<head>\n"+
                    "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\"/>\n"+
                    "<title>403 - Forbidden.</title>\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "<div id=\"header\"><h1>Server Error</h1></div>\n" +
                    "<div id=\"content\">\n" +
                    "<div class=\"content-container\"><fieldset>\n" +
                    "<h2>403 - Forbidden.</h2>\n" +
                    "<h3>You do not have permission to view this resource.</h3>\n" +
                    "</fieldset></div>\n" +
                    "</div>\n" +
                    "</body>\n" +
                    "</html>\n";

    public static final String INTERNAL_ERROR_HTML=
            "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n"+
                    "<head>\n"+
                    "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\"/>\n"+
                    "<title>500 - Internal Error.</title>\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "<div id=\"header\"><h1>Server Error</h1></div>\n" +
                    "<div id=\"content\">\n" +
                    "<div class=\"content-container\"><fieldset>\n" +
                    "<h2>403 - Internal Error.</h2>\n" +
                    "<h3>Internal error occured. Please try again later.</h3>\n" +
                    "</fieldset></div>\n" +
                    "</div>\n" +
                    "</body>\n" +
                    "</html>\n";

}
