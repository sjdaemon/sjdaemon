package com.rgendler.http.request;

import com.rgendler.http.RequestProcessor;
import com.rgendler.server.util.HttpMethod;

/**
 * Factory to obtain {@link RequestProcessor} instance based on Http method.
 *
 *
 * Created by rgendler on 3/15/2017.
 */
public class RequestProcessorFactory {

    /**
     * Static instance
     */
    private static final RequestProcessorFactory INSTANCE = new RequestProcessorFactory();

    /**
     * Private constructor to prevent direct instantiation
     */
    private RequestProcessorFactory() {
    }

    /**
     * Get instance singleton
     * @return  the only instance of {@link RequestProcessorFactory}
     */
    public static RequestProcessorFactory getInstance() {
        return INSTANCE;
    }

    /**
     * Factory method to return instance of {@link RequestProcessor}
     *
     * @param method
     *        {@link HttpMethod} enum value
     *
     * @return
     *        Instance of RequestProcessor
     */
    public RequestProcessor getResponseProcessor(HttpMethod method) {

        switch (method) {
            case GET:
                return new HttpGetProcessorImpl();
            case DELETE:
            case HEAD:
            case PATCH:
            case POST:
            case PUT:
                throw new UnsupportedOperationException();
            default:
                return new HttpGetProcessorImpl();
        }
    }
}
