package com.rgendler.http.request;

import com.rgendler.http.HttpConstants;
import com.rgendler.http.RequestProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.List;
import java.util.Map;

/**
 * Processor for HTTP Get method
 * Does not support any kinf of a dynamic content
 *
 * Created by rgendler on 3/15/2017.
 */
public class HttpGetProcessorImpl implements RequestProcessor {

    private final static Logger logger = LoggerFactory.getLogger(HttpGetProcessorImpl.class);

    /**
     * Constructor
     */
    HttpGetProcessorImpl() {
    }

    @Override
    public void execute(OutputStream output,
                        String path,
                        String serverContentRoot,
                        Map<String, List<String>> parameters) {
        File contentResource = new File(serverContentRoot + path);
        if (!contentResource.exists()) {
            try {
                output.write(("HTTP/1.1 404 Not Found\r\n\r\n").getBytes("UTF-8"));
                output.write(HttpConstants.NOT_FOUND_HTML.getBytes("UTF-8"));
                output.flush();
            } catch (IOException e) {
                logger.error(e.getMessage());
            }

        } else if (contentResource.isFile() && contentResource.canRead()){
            try {
                output.write(("HTTP/1.1 200 OK\r\n").getBytes("UTF-8"));
                output.write(("Content-Type: text/html\r\n").getBytes("UTF-8"));
                output.write(("Connection: keep-alive\r\n\r\n").getBytes("UTF-8"));
                Files.copy(contentResource.toPath(), output);
                output.flush();
            } catch (IOException ioe) {
                ioe.printStackTrace();
                try {
                    output.write(("HTTP/1.1 500 Internal Error\r\n").getBytes("UTF-8"));
                    output.write(HttpConstants.INTERNAL_ERROR_HTML.getBytes("UTF-8"));
                    output.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            try {
                output.write(("HTTP/1.1 403 Forbidden\r\n").getBytes("UTF-8"));
                output.write(HttpConstants.FORBIDDEN_HTML.getBytes("UTF-8"));
                output.flush();
            } catch (IOException e) {
                logger.error(e.getMessage());
            }
        }
    }
}
