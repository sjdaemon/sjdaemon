package com.rgendler.http;

import java.io.OutputStream;
import java.util.List;
import java.util.Map;

/**
 * Interface defining the functionality of HTTP response processing
 * Implementation of this exist for every HTTP method
 *
 * Created by rgendler on 3/12/17.
 */
public interface RequestProcessor {

    /**
     * Process the response
     *
     * @param output
     *        Response output stream
     *
     * @param path
     *        URL Request path
     *
     * @param serverContentRoot
     *        root directory for all content
     *
     * @param parameters
     *        URL parameter values map
     */
    void execute(OutputStream output, String path, String serverContentRoot, Map<String, List<String>> parameters);
}
