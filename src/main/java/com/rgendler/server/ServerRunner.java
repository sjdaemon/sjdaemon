package com.rgendler.server;


import com.rgendler.server.util.HttpServerUtil;

import java.util.Properties;

/**
 * Executable class to start web server
 * Created by rgendler on 3/11/2017.
 */
public class ServerRunner {

    /**
     * Entry point to start web server on a command line
     * @param args
     *        Command line Parameters
     */
    public static void main(String[] args) {
        String propertyFileName = null;
        if (args.length > 0) {
            propertyFileName = args[0];
        } else {
            System.out.println("To include server properties other than default, " +
                    "please include fully qualified path config.properties as command line parameter.");
        }
        Properties properties = HttpServerUtil.getProperties(propertyFileName);
        BasicHttpServer httpServer = new BasicHttpServer(properties);
        new Thread(httpServer).start();
    }


}
