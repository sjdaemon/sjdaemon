package com.rgendler.server.concurrent;

import com.rgendler.http.request.RequestProcessorFactory;
import com.rgendler.server.util.HttpMethod;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.util.HashMap;

/**
 * Web Response Worker Thread class
 *
 * Created by rgendler on 3/12/17.
 */
public class RequestWorkerThread implements Runnable {
    private final static Logger logger = LoggerFactory.getLogger(RequestWorkerThread.class);

    private Socket acceptingSocket = null;
    private String serverContentRoot = null;

    /**
     * Constructor
     * @param acceptingSocket socket that listens for server requests
     */
    public RequestWorkerThread(Socket acceptingSocket, String serverContentRoot) {
        this.acceptingSocket = acceptingSocket;
        this.serverContentRoot = serverContentRoot;
    }

    @Override
    public void run() {
        // Use try-catch with resources to ensure closure of IO resources
        try (InputStream socketInputStream = acceptingSocket.getInputStream();
             OutputStream socketOutputStream = acceptingSocket.getOutputStream()) {

            String[] headerTockens = readRequestHeader(socketInputStream);
            if (headerTockens.length < 3) {
                throw new IllegalArgumentException("Malformed HTTP header.");
            }
            HttpMethod method = HttpMethod.getByName(headerTockens[0]);
            String path = headerTockens[1];
            logger.info("HTTP: " + (method != null ? method.name() : StringUtils.EMPTY));
            logger.info("Path: " + path);

            // delegate request processing
            RequestProcessorFactory.getInstance()
                    .getResponseProcessor(method).execute(socketOutputStream, path, serverContentRoot, new HashMap<>());

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    /**
     * Helper method to parse reqyest header to extract HTTP method and request URI
     * @param inputStream
     *        Socket input stream
     *
     * @return
     *        List of parsed values
     *
     * @throws IOException
     *         For any socket errors
     */
    private String[] readRequestHeader(InputStream inputStream) throws IOException{
        BufferedReader inputBuffredReader = new BufferedReader(new InputStreamReader(inputStream));
        String headerLine = inputBuffredReader.readLine();
        return headerLine.split(StringUtils.SPACE);
    }
}
