package com.rgendler.server.util;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Simple implementation of a thread pool
 *
 * Created by rgendler on 3/12/17.
 */
public class HttpServerUtil {

    private final static Logger logger = LoggerFactory.getLogger(HttpServerUtil.class);

    public static final int DEFAULT_SERVER_PORT = 9080;
    public static final int DEFAULT_REQUEST_NUMBER = 10;
    public static final String DEFAULT_CONTENT_ROOT = ".";

    private static final String DEFAULT_PROPERTIES_FILE = "config.properties";

    /**
     * Helper method to initialize server properties
     *
     * @param propertyFileName
     *        Property file name
     *
     * @return
     *        Java properties object
     */
    public static Properties getProperties(String propertyFileName) {
        Properties properties = null;
        if (StringUtils.isNotBlank(propertyFileName)) {
            File propertyFile = new File(propertyFileName);
            if (propertyFile.exists()) {
                try (InputStream input = new FileInputStream(propertyFileName)) {
                    properties = new Properties();
                    properties.load(input);
                } catch (IOException e) {
                    logger.error("Unable to read load contents of properties file " + propertyFileName
                            + ". Using default properties file " + HttpServerUtil.DEFAULT_PROPERTIES_FILE);
                    propertyFileName = HttpServerUtil.DEFAULT_PROPERTIES_FILE;
                }
            } else {
                logger.error("File " + propertyFileName + " does not exist. Using default properties file "
                        + HttpServerUtil.DEFAULT_PROPERTIES_FILE);
                propertyFileName = HttpServerUtil.DEFAULT_PROPERTIES_FILE;
            }
        } else {
            logger.info("Server properties file is not specified. Using default file name "
                    + HttpServerUtil.DEFAULT_PROPERTIES_FILE);
            propertyFileName = HttpServerUtil.DEFAULT_PROPERTIES_FILE;
        }
        if (properties == null) {
            try (InputStream input = HttpServerUtil.class.getClassLoader().getResourceAsStream(propertyFileName)) {
                properties = new Properties();
                properties.load(input);
            } catch (IOException e) {
                logger.error("Unable to load properties file " + propertyFileName + " as classpath resource.");
            }
        }
        return properties;
    }
}
