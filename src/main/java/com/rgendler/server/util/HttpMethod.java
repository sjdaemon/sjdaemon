package com.rgendler.server.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Enum representing various supported HTTP methods
 *
 * Created by rgendler on 3/17/2017.
 */
public enum HttpMethod {


    DELETE,
    HEAD,
    GET,
    PATCH,
    POST,
    PUT;

    private final static Logger logger = LoggerFactory.getLogger(HttpMethod.class);

    /**
     * Get HttpMethod object by name
     *
     * @param methodName
     *        Method name as string
     *
     * @return
     *        HttpMethod enum
     */
    public static HttpMethod getByName(String methodName) {
        HttpMethod result = null;
        try {
            result = HttpMethod.valueOf(methodName);
        } catch (IllegalArgumentException e) {
            logger.error("Invlaid HTTP Method name: " + methodName);
        }
        return result;
    }
}
