package com.rgendler.server;

import com.rgendler.server.concurrent.RequestWorkerThread;
import com.rgendler.server.util.HttpServerUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Simple HTTP Server implementation that uses
 * {@link java.net.ServerSocket} and thread pool {@link java.util.concurrent.ExecutorService} implementation
 *
 * Created by rgendler on 3/11/17.
 */
public class BasicHttpServer implements Runnable {

    private final static Logger logger = LoggerFactory.getLogger(BasicHttpServer.class);

    private boolean stopped = false;
    private int port;
    private int maxConnectionCount;
    private String serverContentRoot;
    private ServerSocket serverSocket = null;
    // Thread pool
    private ExecutorService workerThreadPoolService = null;

    /**
     * Constructor
     */
    public BasicHttpServer(final Properties properties) {
        if (properties == null) {
            throw new IllegalArgumentException("Properties cannot be null.");
        }
        initializeServerPort(properties);

        initializeMaxNumberConnections(properties);

        String contentRoot = properties.getProperty("server.root.dir");
        if (StringUtils.isBlank(contentRoot)) {
            logger.error("Unable to read value of server.root.dir, using default " + HttpServerUtil.DEFAULT_CONTENT_ROOT);
            contentRoot = HttpServerUtil.DEFAULT_CONTENT_ROOT;
        }
        File contentRootDir = new File(contentRoot);
        String errorMessage;
        if (!contentRootDir.isDirectory()) {
            errorMessage = "Specified content root is not a directory: " + contentRoot;
            logger.error(errorMessage);
            throw new IllegalArgumentException(errorMessage);
        }

        if (!contentRootDir.canRead()) {
            errorMessage = "Specified content root is not a readable directory: " + contentRoot;
            logger.error(errorMessage);
            throw new IllegalArgumentException(errorMessage);
        }

        try {
            this.serverContentRoot = contentRootDir.getCanonicalPath();
        } catch (IOException e) {
            errorMessage = "Unable to create an absolute path for " + contentRoot;
            logger.error(errorMessage);
            throw new IllegalArgumentException(e);
        }

        workerThreadPoolService = Executors.newFixedThreadPool(maxConnectionCount);
    }

    /**
     * Helper method to initialize number of max concurrent connections
     *
     * @param properties
     *        Java properties object
     */
    private void initializeMaxNumberConnections(Properties properties) {
        try {
            this.maxConnectionCount = Integer.parseInt(properties.getProperty("max.request.count"));
        } catch (Exception e) {
            logger.error("Unable to read value of max.request.count, using default " + HttpServerUtil.DEFAULT_REQUEST_NUMBER);
            this.maxConnectionCount = HttpServerUtil.DEFAULT_REQUEST_NUMBER;
        }
    }

    /**
     *  Helper method to initialize the server port
     *
     * @param properties
     *        Java properties object
     */
    private void initializeServerPort(Properties properties) {
        try {
            this.port = Integer.parseInt(properties.getProperty("server.port"));
        } catch (Exception e) {
            logger.error("Unable to read value of server.port, using default " + HttpServerUtil.DEFAULT_SERVER_PORT);
            this.port = HttpServerUtil.DEFAULT_SERVER_PORT;
        }
    }

    public void run() {

        initServer();

        while (!isStopped()) {
            Socket acceptingSocket = initSocket();
            if (acceptingSocket != null) {
                workerThreadPoolService.execute(new RequestWorkerThread(acceptingSocket, serverContentRoot));
            }
        }
        // Shutdown the pool after server is stopped
        workerThreadPoolService.shutdown();
    }

    /**
     * Helper method to initialize HTTP client socket
     *
     * @return
     */
    private Socket initSocket() {
        Socket socket = null;
        try {
            socket = serverSocket.accept();
        } catch (IOException e) {
            logger.error("Unable to accept connections on port: " + port + "\n\n" + e.getMessage());
        }
        return socket;
    }

    /**
     * Helper method to initialize main server socket
     */
    private void initServer() {
        try {
            serverSocket = new ServerSocket(port, maxConnectionCount);

        } catch (IOException ioe) {
            logger.error("Error detected: " + ioe.getMessage());
        }
    }

    public synchronized boolean isStopped() {
        return stopped;
    }

    public synchronized void shutdown(){
        stopped = true;
    }
}
